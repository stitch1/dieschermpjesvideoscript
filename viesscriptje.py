from __future__ import unicode_literals

import logging
import subprocess
import sys
import time

import youtube_dl

# set up logging, which is not straightforward...
log = logging.getLogger()
log.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(lineno)s %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)


log = logging.getLogger("default")


def download(video_code, format):
    class MyLogger(object):
        def debug(self, msg):
            log.debug(msg)

        def warning(self, msg):
            log.warning(msg)

        def error(self, msg):
            log.error(msg)

    def my_hook(d):
        log.debug(f"status = {d['status']}")
        if d['status'] == 'finished':
            log.debug('Done downloading, now converting ...')
            # The filename is incorrect when finished, it's still a temporary name: WOl-YaczcZg.f137.mp4
            # and then it's tried again: The filename is: WOl-YaczcZg.f251.webm

            # file needs to rename and such, even though youtube dl says it is finished, it is not.
            log.debug(f"The filename is: {d['filename']}")

    ydl_opts = {
        'format': format,
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
        # predictable filename without spaces etc.
        'outtmpl': '%(id)s.%(ext)s'
    }

    log.debug(
        f"Downloading youtube video using youtube_dl python: {video_code}")
    download_url = f"https://www.youtube.com/watch?v={video_code}"
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(download_url, download=False)
        video_title = info_dict.get('title', None)
        log.info(f"Downloading {video_title}...")
        ydl.download([download_url])


def convert(video_code, format):

    # because merging takes some time, we'll just wait...
    # BECAUSE THE CALLBACK STATUS IS INCOMPLETE
    time.sleep(2)

    filename = f"{video_code}.{format}"

    ffmpeg = ["ffmpeg"]

    # your precious input file.
    ffmpeg += f"-i {filename}".split(" ")

    # Convert video to mpeg2 video, which is what the machine understands
    ffmpeg += "-c:v mpeg2video".split(" ")

    # The video size of the device is 640x480, and we don't want to stretch to fill, but padd instead.
    ffmpeg += '-vf scale=640:480:force_original_aspect_ratio=decrease,pad=640:480:(ow-iw)/2:(oh-ih)/2'.split(
        " ")

    # We'll remove audio, haha, nope.
    # ffmpeg += ["-an"]

    # use a nice and high bitrate that produces a crisp video
    ffmpeg += "-b:v 5000k".split(" ")

    # and save the whole terribleness to a file, similar to the original filename but with mpg in it's name.
    ffmpeg += [f"{filename}.mpg"]

    log.debug(f"Converting video, with command: {ffmpeg}")
    # copy pastable for debugging in the commandline:
    print(" ".join(ffmpeg))

    # recommended way, giving less PAIN when things go wrong. It actually will help you...
    subprocess.run(ffmpeg)


def main(argv):
    if len(argv) < 1:
        raise ValueError(
            "No youtube video link given. Give one in the format of: 6z21Vw6MUDk")

    log.info("Downloading youtube file to infodisplay...")
    # enjoy your sploits.
    download(argv[0], "mp4")

    # damn! the video merging is NOT a specified state. That means that the postprocessing hook is being called
    # before the video has been merged. It will be called twice, once for audio and once for video.
    # specifying the video format does not solve that, which means we'll just try it seperately using a specified
    # format.
    convert(argv[0], "mp4")


if __name__ == "__main__":
    main(sys.argv[1:])
